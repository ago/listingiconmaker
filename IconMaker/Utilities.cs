﻿// Copyright(c) 2018, 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ListingMaker
{
    public class Utilities
    {
        public static string ByteArrayToString(byte[] ba)
        {
            var hex = new StringBuilder(ba.Length * 2);
            foreach (var b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static async Task<object> TryDownloadAsync(Uri uri)
        {
            try {
                return await DownloadAsync(uri);
            } catch (WebException ex) {
                return ex;
            }
        }

        /* Facebook and other sites like to put the charset in quotes */
        private static readonly char[] _trimChars = new char[] { '"', ' ', '\r', '\n', '\t' };

        public static async Task<string> DownloadAsync(Uri uri)
        {
            var request = WebRequest.CreateHttp(uri);
            var httpRequest = request as HttpWebRequest;
            httpRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
            httpRequest.CookieContainer = new CookieContainer();

            var response = await request.GetResponseAsync() as HttpWebResponse;

            var cs = response.CharacterSet?.Trim(_trimChars);
            var encoding = Encoding.Default;
            if (!string.IsNullOrEmpty(cs))
                encoding = Encoding.GetEncoding(cs);

            using (var responseStream = response.GetResponseStream())
            using (var streamReader = new StreamReader(responseStream, encoding))
                return await streamReader.ReadToEndAsync();
        }

        public static async Task<object> TryDownloadAsync(Uri uri, string outputFileName)
        {
            try {
                return await DownloadAsync(uri, outputFileName);
            } catch (WebException ex) {
                return ex;
            }
        }

        public static async Task<string> DownloadAsync(Uri uri, string outputFileName)
        {
            /* Don't download twice */
            if (File.Exists(outputFileName))
                return outputFileName;

            var request = WebRequest.CreateHttp(uri);
            var httpRequest = request as HttpWebRequest;
            httpRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";

            var response = await request.GetResponseAsync();

            try {
                using (var responseStream = response.GetResponseStream())
#pragma warning disable SCS0018 // Path traversal: injection possible in {1} argument passed to '{0}'
                using (var outputStream = new FileStream(outputFileName, FileMode.CreateNew))
#pragma warning restore SCS0018 // Path traversal: injection possible in {1} argument passed to '{0}'
                    await responseStream.CopyToAsync(outputStream);
            } catch (IOException) {
                /* FIXME: File could already have been created between first check and now in another task */
                return outputFileName;
            }

            return outputFileName;
        }
    }
}
