﻿// Copyright(c) 2018, 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using HtmlAgilityPack;
using ImageMagick;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace ListingMaker
{
    public class IconMaker
    {
        private static readonly int[] _colorCounts;

        static IconMaker()
        {
            /*
             * Generate an array of color counts.
             * We will generate an output image with each of these color counts and find the one with the best or sufficient SSIM
             */
            var levels = new SortedSet<int>();

            var step = 2;
            var current = 0;
            while (current < 72) {
                for (var i = 0; i < 4; i++) {
                    current += step;
                    levels.Add(current);
                }

                step *= 2;
            }

            _colorCounts = levels.ToArray();
        }

        public IconMaker()
        {
            // Output to executable directory by default
            OutputDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        }

        /// <summary>
        /// Output directory
        /// Defaults to the executable directory
        /// </summary>
        public string OutputDirectory { get; set; }

        /// <summary>
        /// Target structural similarity (SSIM)
        /// We try to find an image that has an SSIM greater or equal to this.
        /// If no such image is found we use the image with the highest SSIM.
        /// </summary>
        public double TargetSSIM { get; set; } = 0.97;

        /// <summary>
        /// The background types to generate
        /// </summary>
        public BackgroundType Backgrounds { get; set; } = BackgroundType.White | BackgroundType.Black;

        public Downloader Downloader { get; set; } = new Downloader();

        /// <summary>
        /// Creates candidate icons for a website
        /// </summary>
        /// <param name="name">The short name (id) of the website</param>
        /// <param name="url">The URL of the website</param>
        /// <param name="socialMedia">
        /// A dictionary of social media accounts. Only the account handles, no URLs. Supported keys:
        /// "facebook", "twitter"
        /// </param>
        /// <returns>A list of file names of candidate icons</returns>
        public async Task<IEnumerable<string>> Make(string name, string url, Dictionary<string, string> socialMedia)
        {
            var listingDirectory = Path.Combine(OutputDirectory, name);
            Directory.CreateDirectory(listingDirectory);

            var uri = new Uri(url);
            var baseUri = new Uri(uri.GetLeftPart(UriPartial.Authority));

            var sourceDirectory = Path.Combine(listingDirectory, "source");
            Directory.CreateDirectory(sourceDirectory);
            Downloader.OutputDirectory = sourceDirectory;

            var downloadTasks = new List<Task<IEnumerable<string>>> {
                GetFavicon(baseUri, sourceDirectory)
            };

            try {
                var html = await Utilities.DownloadAsync(baseUri);

                var doc = new HtmlDocument();
                doc.LoadHtml(html);

                downloadTasks.AddRange(new Task<IEnumerable<string>>[] {
                    GetLinkRelIcons(baseUri, doc, sourceDirectory),
                    GetMetaOgImage(baseUri, doc, sourceDirectory),
                    GetImageLogo(baseUri, doc, sourceDirectory)
                });
            } catch (WebException) {
            }

            if (socialMedia.TryGetValue("twitter", out string twitterHandle))
                downloadTasks.Add(GetTwitterImage(twitterHandle, sourceDirectory));

            if (socialMedia.TryGetValue("facebook", out string facebookHandle))
                downloadTasks.Add(GetFacebookImage(facebookHandle, sourceDirectory));

            var res = new List<string>();

            var candidatesDirectory = Path.Combine(listingDirectory, "candidates");
            Directory.CreateDirectory(candidatesDirectory);

            var sourceFiles = (await Task.WhenAll(downloadTasks)).SelectMany(r => r).Distinct().ToList();
            foreach (var imageFileName in sourceFiles) {
                try {
                    res.AddRange(await ProcessImage(imageFileName, candidatesDirectory));
                } catch (MagickErrorException) {
                    Console.WriteLine($"Failed to process image file {imageFileName}, deleting");
                    File.Delete(imageFileName);
                }
            }

            return res;
        }

        private string GetLinkRelOutputName(HtmlNode node)
        {
            var res = node.Attributes["rel"].Value;
            res = res.Replace(' ', '-');
            res = res.Replace('.', '-');

            if (node.Attributes.Contains("sizes"))
                res = res + "-" + node.Attributes["sizes"].Value;

            var href = node.Attributes["href"].Value;

            var qmi = href.IndexOf('?');
            if (qmi > -1)
                href = href.Substring(0, qmi);

            res = res + Path.GetExtension(href);

            return res;
        }

        private Uri GetUriFrom(Uri baseUri, HtmlNode node, string attribute)
        {
            var href = node.Attributes[attribute].Value;

            if (href.StartsWith("//"))
                href = baseUri.Scheme + ":" + href;

            if (href.StartsWith("/"))
                href = baseUri.GetLeftPart(UriPartial.Authority) + href;

            try {
                return new Uri(href);
            } catch (UriFormatException) {
                return new Uri(baseUri + href);
            }
        }

        private static readonly string[] emptyStringArray_ = new string[] { };

        private string RemoveQueryString(string str)
        {
            var idx = str.IndexOf('?');
            if (idx < 0)
                return str;

            return str.Substring(0, idx);
        }

        private async Task<IEnumerable<string>> GetLinkRelIcons(Uri baseUri, HtmlDocument doc, string outputDirectory)
        {
            var links = doc.DocumentNode.SelectNodes("//link")?.Where(l => l.Attributes.Contains("rel") && l.Attributes.Contains("href"));
            if (links == null)
                return emptyStringArray_;

            var iconLinks = links.Where(l => l.Attributes["rel"].Value.Contains("icon"));
            var downloadTasks = iconLinks.Select(l => {
                var uri = GetUriFrom(baseUri, l, "href");
                var ext = Path.GetExtension(RemoveQueryString(uri.ToString()));
                return Downloader.TryDownload("linkicon", uri, ext);
            });

            return (await Task.WhenAll(downloadTasks)).Where(r => r is string).Select(r => r as string);
        }

        private string GetOutputName(HtmlNode node, string attribute)
        {
            var content = node.Attributes[attribute].Value;

            var qmi = content.IndexOf('?');
            if (qmi > -1)
                content = content.Substring(0, qmi);

            return Path.GetFileName(content);
        }

        private async Task<IEnumerable<string>> GetMetaOgImage(Uri baseUri, HtmlDocument doc, string outputDirectory)
        {
            var metas = doc.DocumentNode.SelectNodes("//meta")?.Where(m => m.Attributes.Contains("property") && m.Attributes.Contains("content"));
            if (metas == null)
                return emptyStringArray_;

            var metaLinks = metas.Where(m => m.Attributes["property"].Value.Equals("og:image"));
            var downloadTasks = metaLinks.Select(m => Downloader.TryDownload("megaog", GetUriFrom(baseUri, m, "content")));

            return (await Task.WhenAll(downloadTasks)).Where(r => r is string).Select(r => r as string);
        }

        private async Task<IEnumerable<string>> GetImageLogo(Uri baseUri, HtmlDocument doc, string outputDirectory)
        {
            var images = doc.DocumentNode.SelectNodes("//img")?.Where(i => i.Attributes.Contains("src"));
            if (images == null)
                return emptyStringArray_;

            var logoImages = images.Where(i => i.Attributes["src"].Value.IndexOf("logo", StringComparison.InvariantCultureIgnoreCase) > -1);
            var downloadTasks = logoImages.Select(i => Downloader.TryDownload("logo", GetUriFrom(baseUri, i, "src")));

            return (await Task.WhenAll(downloadTasks)).Where(r => r is string).Select(r => r as string);
        }

        private async Task<IEnumerable<string>> GetFavicon(Uri baseUri, string outputDirectory)
        {
            try {
                var faviconUri = new Uri(baseUri + "favicon.ico");
                var res = await Downloader.Download("favicon", faviconUri, "ico");

                if (new FileInfo(res).Length == 0) {
                    File.Delete(res);
                    return new string[] { };
                }

                return new string[] { res };
            } catch (WebException) {
                return new string[] { };
            }
        }

        private async Task<IEnumerable<string>> GetTwitterImage(string twitterHandle, string outputDirectory)
        {
            var twitterUri = new Uri("https://twitter.com/" + twitterHandle);

            try {
                var html = await Utilities.DownloadAsync(twitterUri);

                var doc = new HtmlDocument();
                doc.LoadHtml(html);

                var avatarNode = doc.DocumentNode?.SelectNodes("//img")?.Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Equals("ProfileAvatar-image "))?.FirstOrDefault();
                if (avatarNode == null)
                    return new string[0];

                return new string[] { await Downloader.Download("twitter", new Uri(avatarNode.Attributes["src"].Value)) };
            } catch (WebException) {
                return new string[0];
            }
        }

        private async Task<IEnumerable<string>> GetFacebookImage(string facebookHandle, string outputDirectory)
        {
            try {
                var facebookUri = new Uri($"https://graph.facebook.com/v3.1/{facebookHandle}/picture");
                return new string[] { await Downloader.Download("facebook", facebookUri) };
            } catch (WebException) {
            }

            try {
                var facebookUri = new Uri($"https://graph.facebook.com/v3.1/{facebookHandle}/picture?redirect=0");
                return new string[] { await Downloader.Download("facebook", facebookUri) };
            } catch (WebException) {
            }

            return new string[] { };
        }

        private static readonly ImageOptimizer _optimizer = new ImageOptimizer() {
            OptimalCompression = true
        };

        private async Task<IEnumerable<string>> ProcessImage(string inputFileName, string outputDirectory)
        {
            var ret = new List<string>();

            if ((Backgrounds & BackgroundType.White) > 0)
                ret.Add(await ProcessImage(inputFileName, outputDirectory, BackgroundType.White));

            if ((Backgrounds & BackgroundType.Black) > 0)
                ret.Add(await ProcessImage(inputFileName, outputDirectory, BackgroundType.Black));

            if ((Backgrounds & BackgroundType.Transparent) > 0)
                ret.Add(await ProcessImage(inputFileName, outputDirectory, BackgroundType.Transparent));

            return ret.ToArray();
        }

        [Flags]
        public enum BackgroundType
        {
            White = (1 << 0),
            Black = (1 << 1),
            Transparent = (1 << 2)
        }

        private async Task<string> ProcessImage(string inputFileName, string outputDirectory, BackgroundType background)
        {

            var outputSubDirectory = Path.Combine(outputDirectory, Enum.GetName(typeof(BackgroundType), background));
            Directory.CreateDirectory(outputSubDirectory);

            var baseImageFileName = await MakeBaseImage(inputFileName, outputSubDirectory, background);

            var quantizedImageFileNames = await Task.WhenAll(_colorCounts.Select(colorCount => QuantizeImage(baseImageFileName, outputSubDirectory, colorCount)));

            var results = await Task.WhenAll(quantizedImageFileNames.Select(fileName => CompareImages(baseImageFileName, fileName)));

            var best = results.Where(r => r.SSIM >= TargetSSIM).FirstOrDefault();

            if (best == null)
                best = results.OrderByDescending(r => r.SSIM).First();

            var discard = results.Except(new CompareResult[] { best });

#pragma warning disable SCS0018 // Path traversal: injection possible in {1} argument passed to '{0}'
            foreach (var r in discard)
                File.Delete(r.FileName);

            File.Delete(baseImageFileName);
#pragma warning restore SCS0018 // Path traversal: injection possible in {1} argument passed to '{0}'

            return best.FileName;
        }

        private async Task<string> MakeBaseImage(string inputFileName, string outputDirectory, BackgroundType background)
        {
            return await Task.Run(() => {
                var name = Path.GetFileNameWithoutExtension(inputFileName);
                var outputFileNameBase = Path.ChangeExtension(name, "png");
                var outputFileName = Path.Combine(outputDirectory, outputFileNameBase);

                if (File.Exists(outputFileName))
                    return outputFileName;

                using (var image = new MagickImage(inputFileName)) {
                    var size = new MagickGeometry(32, 32);

                    image.Strip();
                    image.Resize(size);

                    image.Extent(size, Gravity.Center, MagickColors.Transparent);

                    if (background != BackgroundType.Transparent && image.HasAlpha) {
                        image.BackgroundColor = background == BackgroundType.Black ? MagickColors.Black : MagickColors.White;
                        image.Alpha(AlphaOption.Remove);
                    } else {
                        image.BackgroundColor = MagickColors.Transparent;
                    }

                    image.Write(outputFileName);
                }

                _optimizer.Compress(outputFileName);

                return outputFileName;
            });
        }

        private async Task<string> QuantizeImage(string inputFileName, string outputDirectory, int colors)
        {
            return await Task.Run(() => {
                var name = Path.GetFileNameWithoutExtension(inputFileName) + "_quant_" + colors;
                var outputFileNameBase = Path.ChangeExtension(name, "png");
                var outputFileName = Path.Combine(outputDirectory, outputFileNameBase);

                if (File.Exists(outputFileName))
                    return outputFileName;

                using (var image = new MagickImage(inputFileName)) {
                    image.Quantize(new QuantizeSettings() {
                        Colors = colors,
                        DitherMethod = DitherMethod.FloydSteinberg
                    });

                    image.Write(outputFileName);
                }

                _optimizer.Compress(outputFileName);

                return outputFileName;
            });
        }

        private class CompareResult
        {
            public string ReferenceFileName;
            public string FileName;
            public double SSIM;
        }

        private async Task<CompareResult> CompareImages(string referenceFileName, string fileName)
        {
            return await Task.Run(() => {
                var res = new CompareResult() {
                    ReferenceFileName = referenceFileName,
                    FileName = fileName,
                };

                using (var referenceImage = new MagickImage(referenceFileName))
                using (var image = new MagickImage(fileName))
                    res.SSIM = image.Compare(referenceImage, ErrorMetric.StructuralSimilarity);

                return res;
            });
        }
    }
}
