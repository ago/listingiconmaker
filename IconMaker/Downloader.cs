﻿// Copyright(c) 2018, 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ListingMaker
{
    public class Downloader
    {
        public string OutputDirectory { get; set; }

        public string UserAgent { get; set; } = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";

        public async Task<object> TryDownload(string prefix, Uri uri, string extension = null)
        {
            try {
                return await Download(prefix, uri, extension);
            } catch (WebException ex) {
                return ex;
            }
        }

        private static readonly byte[] emptyBuffer_ = new byte[0];

        private static readonly Regex dataUriRegex_ = new Regex(@"data:image/(?<type>.+?);base64,(?<data>.+)");

        private static readonly Dictionary<string, string> dataUriTypeToExt_ = new Dictionary<string, string>() {
            { "vnd.microsoft.icon", "ico" }
        };

        public async Task<string> Download(string prefix, Uri uri, string extension = null)
        {
            if ("data".Equals(uri.Scheme)) {
                /* Handle data Uris */
                var match = dataUriRegex_.Match(uri.ToString());
                if (!match.Success)
                    throw new ArgumentException("Invalid data Uri", nameof(uri));

                var data = Convert.FromBase64String(match.Groups["data"].Value);
#pragma warning disable SCS0006 // Weak hashing function
                var hash = Utilities.ByteArrayToString(MD5.Create().ComputeHash(data));
#pragma warning restore SCS0006 // Weak hashing function

                var outputFileName = Path.Combine(OutputDirectory, $"{prefix}_{hash}");
                if (dataUriTypeToExt_.TryGetValue(match.Groups["type"].Value, out string ext))
                    outputFileName = Path.ChangeExtension(outputFileName, ext);

                if (File.Exists(outputFileName))
                    return outputFileName;

                File.WriteAllBytes(outputFileName, data);
                return outputFileName;
            }

            var request = WebRequest.CreateHttp(uri);
            var httpRequest = request as HttpWebRequest;
            httpRequest.UserAgent = UserAgent;

            var tempFile = Path.GetTempFileName();

            var response = await request.GetResponseAsync();

#pragma warning disable SCS0006 // Weak hashing function
            var md5 = MD5.Create();
#pragma warning restore SCS0006 // Weak hashing function

            try {
                var buf = new byte[8192];
                using (var responseStream = response.GetResponseStream())
                using (var outputStream = new FileStream(tempFile, FileMode.Open, FileAccess.Write)) {
                    while (true) {
                        var read = await responseStream.ReadAsync(buf, 0, buf.Length);
                        if (read == 0)
                            break;
                        md5.TransformBlock(buf, 0, read, null, 0);
                        await outputStream.WriteAsync(buf, 0, read);
                    }
                }
                md5.TransformFinalBlock(emptyBuffer_, 0, 0);

                var hash = Utilities.ByteArrayToString(md5.Hash);
                var outputFileName = Path.Combine(OutputDirectory, $"{prefix}_{hash}");
                if (!string.IsNullOrWhiteSpace(extension))
                    outputFileName = Path.ChangeExtension(outputFileName, extension);

                if (File.Exists(outputFileName)) {
                    File.Delete(tempFile);
                    return outputFileName;
                }

                File.Move(tempFile, outputFileName);
                return outputFileName;
            } catch {
                File.Delete(tempFile);
                throw;
            }
        }
    }
}
