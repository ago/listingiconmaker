﻿// Copyright(c) 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

namespace ListingMaker
{
    public class Region
    {
        public string Name { get; private set; }
        public string Code { get; private set; }

        private Region(string name, string code)
        {
            Name = name;
            Code = code;
        }

        public static readonly Region[] List = new[]
        {
            new Region("Africa", "af"),
            new Region("North America", "na"),
            new Region("Oceania", "oc"),
            new Region("Antarctica", "an"),
            new Region("Asia", "as"),
            new Region("Europe", "eu"),
            new Region("South America", "sa"),
        };
    }
}
