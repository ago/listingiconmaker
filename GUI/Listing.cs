﻿// Copyright(c) 2018, 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System.ComponentModel;
using YamlDotNet.Serialization;

namespace ListingMaker
{
    public class Listing : INotifyPropertyChanged
    {
#pragma warning disable 67 // Fody PropertChanged
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67

        public Listing()
        {
            AcceptsBCH = false;
            AcceptsBTC = false;
            AcceptsOther = false;
        }

        [YamlMember(Alias = "name")]
        public string Name { get; set; }

        [YamlMember(Alias = "url")]
        public string Url { get; set; }

        [YamlIgnore]
        public string NameShort { get; set; }

        [YamlMember(Alias = "img")]
        public string Image {
            get => $"{NameShort}.png";
        }

        [YamlMember(Alias = "twitter")]
        public string Twitter { get; set; }

        [YamlMember(Alias = "facebook")]
        public string Facebook { get; set; }

        [YamlMember(Alias = "email_address")]
        public string Email { get; set; }

        [YamlMember(Alias = "bch")]
        public bool? AcceptsBCH { get; set; }

        [YamlMember(Alias = "btc")]
        public bool? AcceptsBTC { get; set; }

        [YamlMember(Alias = "bsv")]
        public bool? AcceptsBSV { get; set; }

        [YamlMember(Alias = "othercrypto")]
        public bool? AcceptsOther { get; set; }

        [YamlIgnore]
        public Region Region { get; set; }

        [YamlMember(Alias = "region")]
        public string RegionYAML => Region?.Code;

        [YamlIgnore]
        public ISO3166.Country Country { get; set; }

        [YamlMember(Alias = "country")]
        public string CountryYAML => Country?.TwoLetterCode;

        [YamlMember(Alias = "state")]
        public string State { get; set; }

        [YamlMember(Alias = "city")]
        public string City { get; set; }

        [YamlMember(Alias = "lang")]
        public string Language { get; set; }

        [YamlMember(Alias = "doc")]
        public string Doc { get; set; }

        [YamlMember(Alias = "exceptions")]
        public string Exceptions { get; set; }

        [YamlMember(Alias = "keywords")]
        public string[] Keywords { get; set; }
    }
}
