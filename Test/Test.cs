﻿// Copyright(c) 2018, 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System.Collections.Generic;
using System.Threading.Tasks;

namespace ListingMaker
{
    class Test
    {
        static async Task Main(string[] args)
        {
            var lim = new IconMaker();

            //await lim.Make("dreamruns", "https://dreamruns.com/", new Dictionary<string, string>() {
            //    { "facebook", "dreamruns" },
            //});

            await lim.Make("truenudists", "https://www.truenudists.com/", new Dictionary<string, string>() {
                { "facebook", "True-Nudists-184557604923252" },
                { "twitter", "truenudists" },
            });

            await lim.Make("trueswingers", "https://www.trueswingers.com/", new Dictionary<string, string>() {
                { "facebook", "TrueSwingerscom" },
                { "twitter", "trueswingers" },
            });
        }
    }
}
